extends ProgressBar

var audio_up
var audio_dano

func muda_valor():
	value -= 5
	audio_dano.play()

func aumenta_valor():
	value += 5	
	audio_up.play()

func _ready():
	var jogador = get_node("/root/principal/Jogador/jogador")
	audio_up = get_node("/root/principal/audio_up")
	audio_dano = get_node("/root/principal/audio_dano")
	jogador.connect("tomou_dano", self.muda_valor)
	jogador.connect("power_up", self.aumenta_valor)
	
	
