extends CharacterBody2D

var speed = 100
var gravidade = 800
var direction = -3

func _physics_process(delta):
	if not is_on_floor():
		velocity.y += delta * gravidade
	velocity.x += direction
	
	move_and_slide()
