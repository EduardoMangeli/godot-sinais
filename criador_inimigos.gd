extends Node2D

var padrao = preload("res://inimigo.tscn")
@export var max_inimigos = 10
var inimigos = 0
var random = RandomNumberGenerator.new()


func cria_inimigo():
	if inimigos < max_inimigos:
		position.x += random.randi_range(-500, + 50)
		var inimigo = padrao.instantiate()
		add_child(inimigo)
		inimigo.global_position = self.global_position
		inimigos += 1
	
func _ready():
	random.randomize()
	var timer = Timer.new()
	timer.wait_time = 2.0  # Tempo em segundos entre spawns.
	timer.connect("timeout", self.cria_inimigo)
	timer.autostart = true
	add_child(timer)
	
