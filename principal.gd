extends Node2D

const plat = preload("res://plataforma.tscn")
const powerUP = preload("res://power_up.tscn")
var uma_plat
var um_power

func _ready():
	$audio_fundo.play()

	var tamanho = 50
	
	"""var contador = 0
	while contador < 22:
		uma_plat = plat.instantiate()
		uma_plat.position.y = 625
		uma_plat.position.x = tamanho * (contador + 1) - 25
		self.add_child(uma_plat)
		contador += 1
	
	var locais = [
		1,1,1,1,1,
		1,0,0,0,1,
		1,1,1,1,1,
		1,1,1,1,1,1,1
	]
	"""
	
	var locais = [
		[1,1,2,1,1,0,0,0,0,0,1,1,1,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
		[0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,],
	]
	
	var posicao_horizontal_inicial = -1000
	var posicao_vertical_inicial = 170
	var contador_coluna
	var contador_linha = 0
	
	for linha in locais:
		contador_coluna = 0
		for coluna in linha:
			if coluna > 0:
				uma_plat = plat.instantiate()
				uma_plat.position.y = posicao_vertical_inicial + (contador_linha + 1) * 200
				uma_plat.position.x = tamanho * (contador_coluna + 1) - 25 + posicao_horizontal_inicial
				self.add_child(uma_plat)
				if coluna == 2:
					um_power = powerUP.instantiate()
					um_power.position.x = uma_plat.position.x
					um_power.position.y = uma_plat.position.y - 70
					self.add_child(um_power)
			contador_coluna += 1
		contador_linha += 1
			
